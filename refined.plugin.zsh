# Simple prompt that does everything I need. No git/docker/mercurial/dropbox/crypto integrations, very quick and snappy.

local -r \
  execution_status_color="%(?.%F{#50a14f}.%F{#ca1243})"\
  sudo_or_regular=" %(!.#.)"

# allows you to put functions in the prompt
setopt prompt_subst

# RPROMPT should disappear on enter (we use this for time)
setopt transient_rprompt

# print newline before each prompt after the first
# Source https://stackoverflow.com/a/60790101/1198836
precmd() $funcstack[1]() echo

# show time on rhs after starting a command
# source: https://stackoverflow.com/a/26585789/1198836
strlen () {
    FOO=$1
    local zero='%([BSUbfksu]|([FB]|){*})'
    LEN=${#${(S%%)FOO//$~zero/}}
    echo $LEN
}

# show right prompt with date ONLY when command is executed
preexec () {
    DATE=$( date +"[%H:%M:%S]" )
    local len_right=$( strlen "$DATE" )
    len_right=$(( $len_right+1 ))
    local right_start=$(($COLUMNS - $len_right))

    local len_cmd=$( strlen "$@" )
    local len_prompt=$(strlen "$PROMPT" )
    local len_left=$(($len_cmd+$len_prompt))

    RDATE="\033[${right_start}C ${DATE}"

    echo -e "\033[1A${RDATE}"
}

# Show icon if there's a working jobs in the background
# Source https://github.com/spaceship-prompt/spaceship-prompt/blob/master/sections/jobs.zsh
background_jobs() {
  local jobs_amount=$(jobs -d | awk '!/pwd/' | wc -l | tr -d " ")

  # echo $jobs_amount
  if [[ $jobs_amount -le 0 ]] 
  then
    echo ""
  else
    echo "✦"
  fi
}

PROMPT="$execution_status_color$sudo_or_regular %f"
# backgrounded jobs and current filepath on RHS
RPROMPT='$(background_jobs) %2~'
