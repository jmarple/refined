# refined

Simple ZSH prompt, based off of https://github.com/spaceship-prompt/spaceship-prompt and https://github.com/sindresorhus/pure.

# Installation
Install with Znap (https://github.com/marlonrichert/zsh-snap):

```
znap prompt https://gitlab.com/jmarple/refined
```

Or install manually by cloning the repo and source the file into your `.zshrc`:

```
source $path_to_repo/refined/refined.plugin.zsh
```

# License 
This project is licensed under the MIT License.
